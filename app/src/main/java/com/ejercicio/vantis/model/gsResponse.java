package com.ejercicio.vantis.model;

public class gsResponse {
    private int idCheckRegister;
    private int idUser;
    private String checkDate;
    private int idCheckType;
    private String deviceToken;
    private String serverDate;
    private String checkLatLong;
    private String checkComments;
    private int idUserAssignment;
    private int idCheckReasons;
    private int idLastCheck;
    private boolean idEmployee;

    public gsResponse(int SidCheckRegister, int sidUser, String scheckDate, int sidCheckType, String sdeviceToken, String sserverDate, String scheckLatLong, String scheckComments, int sidUserAssignment, int sidCheckReasons, int sidLastCheck, boolean sidEmployee) {
        idCheckRegister = SidCheckRegister;
        idUser = sidUser;
        checkDate = scheckDate;
        idCheckType = sidCheckType;
        deviceToken = sdeviceToken;
        serverDate = sserverDate;
        checkLatLong = scheckLatLong;
        checkComments = scheckComments;
        idUserAssignment = sidUserAssignment;
        idCheckReasons = sidCheckReasons;
        idLastCheck = sidLastCheck;
        idEmployee = sidEmployee;
    }

    public int getIdCheckRegister() {
        return idCheckRegister;
    }

    public void setIdCheckRegister(int idCheckRegister) {
        this.idCheckRegister = idCheckRegister;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public String getCheckDate() {
        return checkDate;
    }

    public void setCheckDate(String checkDate) {
        this.checkDate = checkDate;
    }

    public int getIdCheckType() {
        return idCheckType;
    }

    public void setIdCheckType(int idCheckType) {
        this.idCheckType = idCheckType;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getServerDate() {
        return serverDate;
    }

    public void setServerDate(String serverDate) {
        this.serverDate = serverDate;
    }

    public String getCheckLatLong() {
        return checkLatLong;
    }

    public void setCheckLatLong(String checkLatLong) {
        this.checkLatLong = checkLatLong;
    }

    public String getCheckComments() {
        return checkComments;
    }

    public void setCheckComments(String checkComments) {
        this.checkComments = checkComments;
    }

    public int getIdUserAssignment() {
        return idUserAssignment;
    }

    public void setIdUserAssignment(int idUserAssignment) {
        this.idUserAssignment = idUserAssignment;
    }

    public int getIdCheckReasons() {
        return idCheckReasons;
    }

    public void setIdCheckReasons(int idCheckReasons) {
        this.idCheckReasons = idCheckReasons;
    }

    public int getIdLastCheck() {
        return idLastCheck;
    }

    public void setIdLastCheck(int idLastCheck) {
        this.idLastCheck = idLastCheck;
    }

    public boolean isIdEmployee() {
        return idEmployee;
    }

    public void setIdEmployee(boolean idEmployee) {
        this.idEmployee = idEmployee;
    }
}
