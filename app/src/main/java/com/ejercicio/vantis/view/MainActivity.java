package com.ejercicio.vantis.view;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.ejercicio.vantis.R;
import com.ejercicio.vantis.model.gsResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity {

    TextView boxFF;
    TextView boxFI;
    Button btnGenerate;
    int textMessage = R.string.app_name;
    String sBoxFI;
    String sBoxFF;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        boxFI = findViewById(R.id.boxFI);
        boxFF = findViewById(R.id.boxFF);
        btnGenerate = findViewById(R.id.btnGenerate);

        // Acción bóton generar
        btnGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sBoxFI = boxFI.getText().toString();
                sBoxFF = boxFF.getText().toString();

                if (!sBoxFI.isEmpty() || !sBoxFF.isEmpty()) {
                    if (convertDateToLong(sBoxFI) < convertDateToLong(sBoxFF) || convertDateToLong(sBoxFI) == convertDateToLong(sBoxFF)) {
                        if (convertDateToLong(sBoxFF) < convertCurrentDateToLong()) {

                            String url = "http://35.236.88.26:8074/vantischeckin-services/rest/History/?idUser=379&dateStarFind" + sBoxFI + "&dateEndFind" + sBoxFF;
                            sendAndRequestResponse(url);


                        } else textMessage = R.string.mgsFilter2;
                    } else textMessage = R.string.mgsFilter1;
                } else textMessage = R.string.mgsEmptyField;


                Toast.makeText(getBaseContext(), textMessage, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public long convertDateToLong(String convertDate) {
        Date date = new Date();
        @SuppressLint("SimpleDateFormat") SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = formatter.parse(convertDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public long convertCurrentDateToLong() {
        Date date = new Date();
        return date.getTime();
    }

    private void sendAndRequestResponse(String url) {
        RequestQueue mRequestQueue = Volley.newRequestQueue(getApplicationContext());
          StringRequest mStringRequest = new StringRequest(Request.Method.GET, url, response -> {
                  try {
                        //getting the whole json object from the response
                        JSONObject obj = new JSONObject(response);
                        JSONArray dataArray  = obj.getJSONArray("responseObject");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataobj = dataArray.getJSONObject(i);

                            gsResponse gs = new gsResponse(dataobj.getInt("idCheckRegister"), dataobj.getInt("idUser"), dataobj.getString("checkDate"), dataobj.getInt("idCheckType"), dataobj.getString("deviceToken"), dataobj.getString("serverDate"), dataobj.getString("checkLatLong"), dataobj.getString("checkComments"), dataobj.getInt("idUserAssignment"), dataobj.getInt("idCheckReasons"), dataobj.getInt("idLastCheck"), dataobj.getBoolean("idEmployee"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Try again", Toast.LENGTH_LONG).show();
                    }
                }, error -> {
                    Toast.makeText(getApplicationContext(), "Error Detected", Toast.LENGTH_LONG).show();
                });
        mRequestQueue.add(mStringRequest);
    }
    // Me ocurrio un error aqui, ya no me dio tiempo de encontrarlo, pero basicamente la idea de lo que pretendia hacer esta plasmada, envie los datos por medio de get (suponinedo que era ese el formato).
}
